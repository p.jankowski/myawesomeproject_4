with open('hello.txt', 'w') as f:
    f.write('hello, world!')


f = open('hello.txt', 'w')
try:
    f.write('hello world!')
finally:
    f.close()
