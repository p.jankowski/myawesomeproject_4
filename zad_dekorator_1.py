def decorator(obj):
    def wrapper():
        obj()
        print("world")

    return wrapper


@decorator
def function():
    print("hello")


function()
