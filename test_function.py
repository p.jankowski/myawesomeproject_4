from unittest import TestCase


class TestFunction(TestCase):
    def test_function(self):
        self.assertTrue("hello" == "hello")


class TestWrapper(TestCase):
    def test_wrapper(self):
        self.assertTrue("world" == "world")
