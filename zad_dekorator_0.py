from datetime import datetime


def disable(func):
    def wrapper():
        if 6 <= datetime.now().hour < 22:
            func()
        else:
            pass

    return wrapper


@disable
def say_something():
    print("Hello!!")


say_something()
